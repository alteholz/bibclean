/* WARNING: Do NOT edit this file.  It was created automatically echo    with the command "make bibclean.h" by beebe@eta.math.utah.edu echo    in /u/sy/beebe/tex/bibclean/bibclean-3.05 on Mon May 18 08:40:00 MDT 2020 */

	"  Command-line switches may be abbreviated to a  unique  leading  prefix,\n",
	"  and  letter case is not significant.  All options are parsed before any\n",
	"  input bibliography files are read, no matter what their  order  on  the\n",
	"  command  line.   Options  that correspond to a yes/no setting of a flag\n",
	"  have a form with a prefix \"no-\" to  set  the  flag  to  no.   For  such\n",
	"  options,  the  last  setting  determines  the flag value used.  That is\n",
	"  significant when options are also  specified  in  initialization  files\n",
	"  (see the INITIALIZATION FILES manual section).\n",
	"\n",
	"  The  leading hyphen that distinguishes an option from a filename may be\n",
	"  doubled, for compatibility  with  GNU  and  POSIX  conventions.   Thus,\n",
	"  -author and --author are equivalent.\n",
	"\n",
	"  To avoid confusion with options, if a filename begins with a hyphen, it\n",
	"  must be disguised by a leading absolute  or  relative  directory  path,\n",
	"  e.g., /tmp/-foo.bib or ./-foo.bib.\n",
	"\n",
	"  -author   Display  an author credit on the standard error unit, stderr,\n",
	"            and then terminate with a success return code.  Sometimes  an\n",
	"            executable  program  is  separated from its documentation and\n",
	"            source code; this option provides a way to recover from that.\n",
	"\n",
	"  -copyleft Display copyright information on  the  standard  error  unit,\n",
	"            stderr, and then terminate with a success return code.\n",
	"\n",
	"  -copyright\n",
	"            Display  copyright  information  on  the standard error unit,\n",
	"            stderr, and then terminate with a success return code.\n",
	"\n",
	"  -error-log filename\n",
	"            Redirect stderr to the indicated file,  which  then  contains\n",
	"            all  of  the  error  and  warning  messages.   This option is\n",
	"            provided for those systems that have  difficulty  redirecting\n",
	"            stderr.\n",
	"\n",
	"  -help or -?\n",
	"            Display a help message on stderr, giving a usage description,\n",
	"            similar to  this  section  of  the  manual  pages,  and  then\n",
	"            terminate with a success return code.\n",
	"\n",
	"  -ISBN-file filename\n",
	"            Provide  an  explicit  ISBN-range initialization file.  It is\n",
	"            processed   after   any   system-wide   and   job-wide   ISBN\n",
	"            initialization   files  found  on  the  PATH  (for  VAX  VMS,\n",
	"            SYS$SYSTEM) and BIBINPUTS search paths, respectively, and may\n",
	"            override  them.   The  ISBN  initialization  file name can be\n",
	"            changed at compile time, or at run time through a setting  of\n",
	"            the   environment  variable  BIBCLEANISBN,  but  defaults  to\n",
	"            .bibclean.isbn on  UNIX,  and  bibclean.isb  elsewhere.   For\n",
	"            further  details,  see  the  ISBN INITIALIZATION FILES manual\n",
	"            section.\n",
	"\n",
	"  -init-file filename\n",
	"            Provide an explicit value pattern initialization file.  It is\n",
	"            processed  after  any system-wide and job-wide initialization\n",
	"            files found  on  the  PATH  (for  VAX  VMS,  SYS$SYSTEM)  and\n",
	"            BIBINPUTS  search paths, respectively, and may override them.\n",
	"            It in turn may be overridden by  a  subsequent  file-specific\n",
	"            initialization  file.   The  initialization  file name can be\n",
	"            changed at compile time, or at run time through a setting  of\n",
	"            the   environment   variable  BIBCLEANINI,  but  defaults  to\n",
	"            .bibcleanrc on UNIX,  and  to  bibclean.ini  elsewhere.   For\n",
	"            further details, see the INITIALIZATION FILES manual section.\n",
	"\n",
	"  -keyword-file filename\n",
	"            Provide  an  explicit  keyword  initialization  file.   It is\n",
	"            processed  after  any  system-wide   and   job-wide   keyword\n",
	"            initialization   files  found  on  the  PATH  (for  VAX  VMS,\n",
	"            SYS$SYSTEM) and BIBINPUTS search paths, respectively, and may\n",
	"            override  them.   The keyword initialization file name can be\n",
	"            changed at compile time, or at run time through a setting  of\n",
	"            the   environment   variable  BIBCLEANKEY,  but  defaults  to\n",
	"            .bibclean.key  on  UNIX,  and  bibclean.key  elsewhere.   For\n",
	"            further  details, see the KEYWORD INITIALIZATION FILES manual\n",
	"            section.\n",
	"\n",
	"  -max-width nnn\n",
	"            bibclean normally limits output line widths to 72 characters,\n",
	"            and in the interests of consistency, that value should not be\n",
	"            changed.   Occasionally,  special-purpose  applications   may\n",
	"            require   different  maximum  line  widths,  so  this  option\n",
	"            provides that capability.  The number  following  the  option\n",
	"            name can be specified in decimal, octal (starting with 0), or\n",
	"            hexadecimal (starting with 0x).  A zero or negative value  is\n",
	"            interpreted to mean unlimited, so -max-width 0 can be used to\n",
	"            ensure that each field/value pair appears on a single line.\n",
	"\n",
	"            When -no-prettyprint requests bibclean to act  as  a  lexical\n",
	"            analyzer,   the  default  line  width  is  unlimited,  unless\n",
	"            overridden by this option.\n",
	"\n",
	"            When bibclean is prettyprinting, line wrapping is  done  only\n",
	"            at a space. Consequently, a long non-blank character sequence\n",
	"            may result in the output exceeding the requested line width.\n",
	"\n",
	"            When bibclean is lexing, line wrapping is done by inserting a\n",
	"            backslash-newline pair when the specified maximum is reached,\n",
	"            so no line length ever exceeds the maximum.\n",
	"\n",
	"  -[no-]align-equals\n",
	"            With the positive form, align the equals  sign  in  key/value\n",
	"            assignments  at  the same column, separated by a single space\n",
	"            from the value string.  Otherwise, the  equals  sign  follows\n",
	"            the key, separated by a single space.  Default: no.\n",
	"\n",
	"  -[no-]brace-protect\n",
	"            Protect  uppercase  and  mixedcase  words at brace-level zero\n",
	"            with braces to prevent  downcasing  by  some  BibTeX  styles.\n",
	"            Default: yes.\n",
	"\n",
	"  -[no-]check-values\n",
	"            With  the  positive form, apply heuristic pattern matching to\n",
	"            field values in order to detect possible errors (e.g., ``year\n",
	"            =  \"192\"''  instead of ``year = \"1992\"''), and issue warnings\n",
	"            when unexpected patterns are found.\n",
	"\n",
	"            That checking is usually beneficial, but if it  produces  too\n",
	"            many  bogus  warnings for a particular bibliography file, you\n",
	"            can disable  it  with  the  negative  form  of  this  option.\n",
	"            Default: yes.\n",
	"\n",
	"  -[no-]debug-match-failures\n",
	"            With  the  positive  form,  print  out a warning when a value\n",
	"            pattern fails to match a value string.\n",
	"\n",
	"            That is helpful in debugging new patterns,  but  because  the\n",
	"            output  can  be  voluminous,  you should use this option only\n",
	"            with  small  test  files,  and  initialization   files   that\n",
	"            eliminate  all  patterns  apart  from  the  ones that you are\n",
	"            testing.  Default: no.\n",
	"\n",
	"  -[no-]delete-empty-values\n",
	"            With the positive form,  remove  all  field/value  pairs  for\n",
	"            which  the  value  is  an  empty  string.  That is helpful in\n",
	"            cleaning  up  bibliographies  generated  from   text   editor\n",
	"            templates. Compare this option with -[no-]remove-OPT-prefixes\n",
	"            described below.  Default: no.\n",
	"\n",
	"  -[no-]file-position\n",
	"            With  the  positive  form,  give   detailed   file   position\n",
	"            information in warning and error messages.  Default: no.\n",
	"\n",
	"  -[no-]fix-accents\n",
	"            With  the  positive  form,  normalize TeX accents in annotes,\n",
	"            authors, booktitles, editors,  notes,  remarks,  and  titles.\n",
	"            Default: no.\n",
	"\n",
	"  -[no-]fix-braces\n",
	"            With   the  positive  form,  normalize  bracing  in  annotes,\n",
	"            authors, booktitles, editors, notes, remarks, and titles,  by\n",
	"            removing unnecessary levels of braces.  Default: no.\n",
	"\n",
	"  -[no-]fix-degrees\n",
	"            With the positive form, remove spaces in author/editor fields\n",
	"            inside  braces  after  letter-ending  periods.   That   makes\n",
	"            reductions  from  J. J. {Thomson, M. A., F. R. S.}, Frederick\n",
	"            {Soddy, B. A. (Oxon.)}, and John A. {Cable, M.  A.,  M.  Ed.,\n",
	"            Dipl.  Deutsch  (Marburg),  A.  L.  C. M.} to J. J. {Thomson,\n",
	"            M.A., F.R.S.}, Frederick {Soddy, B.A. (Oxon.)}, and  John  A.\n",
	"            {Cable,   M.A.,  M.Ed.,  Dipl.Deutsch  (Marburg),  A.L.C.M.},\n",
	"            respectively.\n",
	"\n",
	"            In journals in the humanities and history of science, as well\n",
	"            as  in  some  scientific  journals  until  well into the 20th\n",
	"            Century, academic,  honorary,  and  professional  titles  and\n",
	"            degrees are commonly attached to personal names.  Even though\n",
	"            modern  publishing  practice  avoids  such  decorations,  for\n",
	"            accuracy, bibliography entries should preferably retain them.\n",
	"            Journal  typographical   practice   generally   follows   the\n",
	"            reductions described here.\n",
	"\n",
	"  -[no-]fix-font-changes\n",
	"            With  the  positive  form,  supply  an additional brace level\n",
	"            around font changes in titles to protect  against  downcasing\n",
	"            by  some  BibTeX styles.  Font changes that already have more\n",
	"            than one level of braces are not modified.\n",
	"\n",
	"            For example, if  a  title  contains  the  Latin  phrase  {\\em\n",
	"            Dictyostelium    discoideum}    or    {\\em    {D}ictyostelium\n",
	"            discoideum}, then downcasing incorrectly converts the  phrase\n",
	"            to  lower-case letters.  Most BibTeX users are surprised that\n",
	"            bracing the initial letters does  not  prevent  the  downcase\n",
	"            action.    The   correct   coding   is   {{\\em  Dictyostelium\n",
	"            discoideum}}.  However, there are also legitimate cases where\n",
	"            an  extra  level of bracing wrongly protects from downcasing.\n",
	"            Consequently, bibclean normally  does  not  supply  an  extra\n",
	"            level  of  braces,  but  if you have a bibliography where the\n",
	"            extra braces are routinely missing, you can use  this  option\n",
	"            to supply them.\n",
	"\n",
	"            If  you  think  that  you  need  this  option, it is strongly\n",
	"            recommended that you apply bibclean to your bibliography file\n",
	"            with  and  without  -fix-font-changes,  then  compare the two\n",
	"            output files to  ensure  that  extra  braces  are  not  being\n",
	"            supplied  in  titles  where  they should not be present.  You\n",
	"            must decide which of the  two  output  files  is  the  better\n",
	"            choice, then repair the incorrect title bracing by hand.\n",
	"\n",
	"            Because font changes in titles are uncommon, except for cases\n",
	"            of the type that this  option  is  designed  to  correct,  it\n",
	"            should do more good than harm.  Default: no.\n",
	"\n",
	"  -[no-]fix-initials\n",
	"            With  the  positive  form,  insert  a  space  after  a period\n",
	"            following author initials.  Default: yes.\n",
	"\n",
	"  -[no-]fix-math\n",
	"            With the positive form, improve readability of math  mode  in\n",
	"            titles  by  inserting spaces around operators, deleting other\n",
	"            unnecessary  space,  and  removing  braces   around   single-\n",
	"            character subscripts and superscripts.  Default: no.\n",
	"\n",
	"  -[no-]fix-names\n",
	"            With  the positive form, reorder author and editor name lists\n",
	"            to remove commas at brace level zero, placing first names  or\n",
	"            initials before last names.  Default: yes.\n",
	"\n",
	"  -[no-]German-style\n",
	"            With the positive form, interpret quote characters [\"] inside\n",
	"            braced value strings  at  brace  level  1  according  to  the\n",
	"            conventions of the TeX style file german.sty, which overloads\n",
	"            quote to simplify input and representation of  German  umlaut\n",
	"            accents,  sharp-s  (es-zet),  ligature  separators, invisible\n",
	"            hyphens,  raised/lowered  quotes,  French   guillemets,   and\n",
	"            discretionary hyphens.  Recognized character combinations are\n",
	"            braced to prevent BibTeX from interpreting  the  quote  as  a\n",
	"            string delimiter.\n",
	"\n",
	"            Quoted  strings receive no special handling from this option,\n",
	"            and because German nouns in titles must anyway  be  protected\n",
	"            from  the  downcasing  operation  of most BibTeX bibliography\n",
	"            styles, German value strings that use  the  overloaded  quote\n",
	"            character  can always be entered in the form \"{...}\", without\n",
	"            the need to specify this option at all.\n",
	"\n",
	"            Default: no.\n",
	"\n",
	"  -[no-]keep-linebreaks\n",
	"            Normally, line breaks inside value strings are collapsed into\n",
	"            a  single  space,  so  that  long  value strings can later be\n",
	"            broken to provide lines of reasonable length.\n",
	"\n",
	"            With the positive form, linebreaks  are  preserved  in  value\n",
	"            strings.   If  -max-width  is set to zero, this preserves the\n",
	"            original line breaks.  Spacing outside value strings  remains\n",
	"            under bibclean's control, and is not affected by this option.\n",
	"\n",
	"            Default: no.\n",
	"\n",
	"  -[no-]keep-parbreaks\n",
	"            With  the  positive  form,  preserve paragraph breaks (either\n",
	"            formfeeds, or lines containing only spaces) in value strings.\n",
	"            Normally, paragraph breaks are collapsed into a single space.\n",
	"            Spacing  outside  value  strings  remains  under   bibclean's\n",
	"            control, and is not affected by this option.  Default: no.\n",
	"\n",
	"  -[no-]keep-preamble-spaces\n",
	"            With   the   positive   form,   preserve  all  whitespace  in\n",
	"            @Preamble{...} entries.  Default: no.\n",
	"\n",
	"  -[no-]keep-spaces\n",
	"            With the positive form, preserve all spaces in value strings.\n",
	"            Normally,  multiple spaces are collapsed into a single space.\n",
	"            This option  can  be  used  together  with  -keep-linebreaks,\n",
	"            -keep-parbreaks,  and  -max-width  0  to preserve the form of\n",
	"            value  strings  while  still  providing  syntax   and   value\n",
	"            checking.    Spacing  outside  value  strings  remains  under\n",
	"            bibclean's control, and  is  not  affected  by  this  option.\n",
	"            Default: no.\n",
	"\n",
	"  -[no-]keep-string-spaces\n",
	"            With   the   positive   form,   preserve  all  whitespace  in\n",
	"            @String{...} entries.  Default: no.\n",
	"\n",
	"  -[no-]parbreaks\n",
	"            With the negative form, a paragraph break (either a formfeed,\n",
	"            or  a  line containing only spaces) is not permitted in value\n",
	"            strings, or between field/value pairs.  That may be useful to\n",
	"            quickly   trap   runaway   strings  arising  from  mismatched\n",
	"            delimiters.  Default: yes.\n",
	"\n",
	"  -[no-]prettyprint\n",
	"            Normally, bibclean functions as  a  prettyprinter.   However,\n",
	"            with  the  negative form of this option, it acts as a lexical\n",
	"            analyzer instead, producing a stream of lexical tokens.   See\n",
	"            the  LEXICAL  ANALYSIS  manual  section  for further details.\n",
	"            Default: yes.\n",
	"\n",
	"  -[no-]print-ISBN-table\n",
	"            With the positive form, print the ISBN-range table on stderr,\n",
	"            then terminate with a success return code.\n",
	"\n",
	"            That  action  is  taken  after  all  command-line options are\n",
	"            processed, and before any input files are  read  (other  than\n",
	"            those that are values of command-line options).\n",
	"\n",
	"            The  format  of the output ISBN-range table is acceptable for\n",
	"            input  as  an  ISBN  initialization  file   (see   the   ISBN\n",
	"            INITIALIZATION FILES manual section).  Default: no.\n",
	"\n",
	"  -[no-]print-keyword-table\n",
	"            With  the  positive  form,  print  the keyword initialization\n",
	"            table on stderr, then terminate with a success return code.\n",
	"\n",
	"            That action is  taken  after  all  command-line  options  are\n",
	"            processed,  and  before  any input files are read (other than\n",
	"            those that are values of command-line options).\n",
	"\n",
	"            The format of the output table is acceptable for input  as  a\n",
	"            keyword  initialization  file (see the KEYWORD INITIALIZATION\n",
	"            FILES manual section).  Default: no.\n",
	"\n",
	"  -[no-]print-patterns\n",
	"            With the positive form, print the value  patterns  read  from\n",
	"            initialization  files  as  they are added to internal tables.\n",
	"            Use this option to check newly-added patterns, or to see what\n",
	"            patterns are being used.\n",
	"\n",
	"            When  bibclean  is compiled with native pattern-matching code\n",
	"            (the default), those patterns are the ones that are  used  in\n",
	"            checking  value strings for valid syntax, and all of them are\n",
	"            specified in initialization  files,  rather  than  hard-coded\n",
	"            into   the   program.    For   further   details,   see   the\n",
	"            INITIALIZATION FILES manual section.  Default: no.\n",
	"\n",
	"  -[no-]quiet\n",
	"            This option is the opposite of -[no-]warning; it  exists  for\n",
	"            user  convenience,  and for compatibility with other programs\n",
	"            that use -q  for quiet operation, without warning messages.\n",
	"\n",
	"  -[no-]read-init-files\n",
	"            With the negative form, suppress loading of  system-,  user-,\n",
	"            and file-specific initialization files.  Initializations then\n",
	"            come only from those files  explicitly  given  by  -init-file\n",
	"            filename options.  Default: yes.\n",
	"\n",
	"  -[no-]remove-OPT-prefixes\n",
	"            With  the  positive form, remove the ``OPT'' prefix from each\n",
	"            field name where the corresponding  value  is  not  an  empty\n",
	"            string.  The prefix ``OPT'' must be entirely in upper-case to\n",
	"            be recognized.\n",
	"\n",
	"            This option is for bibliographies generated with the help  of\n",
	"            the   GNU  Emacs  BibTeX  editing  support,  which  generates\n",
	"            templates with optional  fields  identified  by  the  ``OPT''\n",
	"            prefix.  Although the function M-x bibtex-remove-OPT normally\n",
	"            bound to the keystrokes C-c C-o does  the  job,  users  often\n",
	"            forget,  with  the  result that BibTeX does not recognize the\n",
	"            field name, and  ignores  the  value  string.   Compare  this\n",
	"            option   with   -[no-]delete-empty-values   described  above.\n",
	"            Default: no.\n",
	"\n",
	"  -[no-]scribe\n",
	"            With the positive form, accept input syntax conforming to the\n",
	"            Scribe  document  system.  The output is converted to conform\n",
	"            to BibTeX syntax.  See the SCRIBE BIBLIOGRAPHY FORMAT  manual\n",
	"            section for further details.  Default: no.\n",
	"\n",
	"  -[no-]trace-file-opening\n",
	"            With  the  positive  form,  record  in the error log file the\n",
	"            names of all files that bibclean attempts to open.  Use  this\n",
	"            option  to  identify  where initialization files are located.\n",
	"            Default: no.\n",
	"\n",
	"  -[no-]warnings\n",
	"            With the positive form,  allow  all  warning  messages.   The\n",
	"            negative form is not recommended because it may mask problems\n",
	"            that should be repaired.  Default: yes.\n",
	"\n",
	"  -output-file filename\n",
	"            Supply an alternate output file to replace  stdout.   If  the\n",
	"            filename  cannot  be  opened for output, execution terminates\n",
	"            immediately with a nonzero exit code.\n",
	"\n",
	"  -version  Display the  program  version  number  on  stderr,  and  then\n",
	"            terminate  with  a  success  return  code.   That includes an\n",
	"            indication of who compiled the  program,  the  host  name  on\n",
	"            which  it was compiled, the time of compilation, and the type\n",
	"            of string-value matching code selected, when that information\n",
	"            is available to the compiler.\n",
	"\n",
	(const char*)NULL,
